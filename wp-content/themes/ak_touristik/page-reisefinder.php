<?php 
get_header(); 

$ak_header_image_url = get_theme_mod( 'header_image', get_theme_support( 'custom-header', 'default-image' ) );
?>

<div class="ak-header" style="background-image: url('<?php echo ($ak_header_image_url)?>')">
    <?php
    if ( is_active_sidebar( 'reisefinder_search' ) ) : ?>
        <div id="reisefinder_search" class="reisefinder_search">
            <div class="mx-auto">
                <?php dynamic_sidebar( 'reisefinder_search' ); ?>
            </div>
        </div>
    <?php endif;?>

</div>

<?php
/* Get The Loop */
	if ( have_posts() ) : while ( have_posts() ) : the_post();
		get_template_part( 'template-parts/content-notitle', 'page');
	endwhile; endif; 


/*  get_sidebar();  */

 get_footer(); 

?>
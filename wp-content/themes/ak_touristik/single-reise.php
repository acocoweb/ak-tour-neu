<?php
/**
 * The template for displaying all single "Reise"
 *
 * 
 * @package ak_touristik
 */

get_header(); 
?>

<div class="w-100">
    
    <?php while ( have_posts() ) : the_post();
		?>
		<!-- Display the featured image -->
        <div class="reise-hero-image">
        <?php
			if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
				the_post_thumbnail('banner-image');
			}
		?>
		</div>
		<!-- Display any other images of the Reise -->
		<?php
		// check if the repeater field (bilder) has rows of data
			if( have_rows('bilder') ):

				// loop through the rows of data
				while ( have_rows('bilder') ) : the_row();
					$image = get_sub_field('bild');
					// display a sub field value
					?>
					<div class="reise-hero-image ak-hidden">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
					</div>
				<?php 
				endwhile;
			else :
			// no rows (bilder) found
			endif;
		?>

		<div id="ak-reise-sub-menu" class="w-100 reise-sub-nav">
			<div class="container">
				<ul class="reise-sub-nav-menu">
					<li> <a href="#reisebeschreibung" > <?php _e('Reisebeschreibung', 'ak_touristik'); ?> </a></li>
					<li> <a href="#tagesverlauf" > <?php _e('Tagesverlauf', 'ak_touristik'); ?> </a></li>
					<?php if( get_field('leistungen') ) { ?>
						<li> <a href="#leistungen" > <?php _e('Leistungen', 'ak_touristik'); ?> </a> </li>
					<?php } ?>
				</ul>
			</div>
		</div> 

        <div class="container mt-3 mb-3">
			<a class="ak-anchor" id="reisebeschreibung"></a> 
			<div class="row">
                <div class="col-sm-6">
                    <h2 class="reise-title"><?php the_title(); ?></h2>
                    <p class="reise-untertitel"> <?php the_field('reise_untertitel');?> </p>
                    <?php the_excerpt(); ?>
                </div>
                <div class="col-sm-6">
					<p><?php _e( 'Reise merken | Reise drucken | Reise als PDF', 'ak_touristik' ); ?></p>
                    <div class="reise-info">
                        <div>
                        <h3 class="ak-label"><?php _e( 'Termine', 'ak_touristik' ); ?></h3>
                        <?php
                            // check if the repeater field has rows of data
							if( have_rows('daten') ):
								?><p><?php
                                // loop through the rows of data
                                while ( have_rows('daten') ) : the_row();
                                    // display a sub field value
									the_sub_field('von'); ?> - <?php the_sub_field('bis');
									?><br/><?php
								endwhile;
								
                            else :
                                // no rows found
                            endif;
                        ?>
                        </div>

                        <div>
                        <h3 class="ak-label"><?php _e( 'ab Preis p.P.', 'ak_touristik' ); ?></h3>
						<p> &#8364; <?php the_field('ab_preis');?> </p>
						</div>
                    </div>
                    <button type="button" class="btn btn-success ak-buchen-btn"><?php _e( 'Reise buchen', 'ak_touristik' ); ?> &#129130;</button>

                </div>
			</div>
			<a class="ak-anchor" id="tagesverlauf"></a> 
            <div class="row">
                <div class="col-sm-12 reise-beschreibung">
                    <div><?php the_content(); ?></div>
                </div>
			</div> <!-- #tagesverlauf -->
			<a class="ak-anchor" id="leistungen"></a> 
            <div class="row">
				<div class="col-sm-6">
					<?php
					if( get_field('leistungen') ) { ?>
						<div class="reise-info reise-leistungen">
							<div>
							<!--
								Der Titel für "Unsere Leistungen" kommt als <b> in den Daten (nicht als h3) 
								Muss dann speziel angepasst werden
							-->
								<!-- <h3 class="ak-label">
									<?php // _e( 'Unsere Leistungen', 'ak_touristik' ); ?>
								</h3> -->
								<div>
									<?php the_field('leistungen');?> 
								</div>
							</div>
						</div>
					<?php } ?>
                </div>
                <div class="col-sm-6">
				<?php
					if( get_field('hotels') ) { ?>
                    <div class="reise-info">
                        <div>
                            <h3 class="ak-label"><?php _e( 'Ihre Hotels', 'ak_touristik' ); ?></h3>
                            <div>
									<?php the_field('hotels');?> 
							</div>
                        </div>
					</div>
					<?php } ?>
                </div>
			</div> <!--#leistungen -->

			<a class="ak-anchor" id="karte"></a> 
			<div class="row">
                <div class="col-sm-12 reise-karte">
                    <div> </div>
                </div>
            </div>
			
			<div class="row mt-3">
				<div class="col-sm-12">
					<div class="ak-reise-bottom-green-box px-5 pt-5 pb-2">
						<?php
							dynamic_sidebar( 'reise-seite-bottom-info' ); 
						?>	
					</div>
				</div>
			</div>

                            
        </div> <!-- /.container -->
    <?php endwhile; ?>
</div> <!--/.w-100 -->

<?php

get_footer();
?>

<script>
	var slideIndex = 0;  
	ak_carousel();

	function ak_carousel() {
	
		var i;
		var x = document.getElementsByClassName("reise-hero-image");
		for (i = 0; i < x.length; i++) {
			x[i].style.display = "none";
		}	
		slideIndex++;
		if (slideIndex > x.length) {
			slideIndex = 1
		}
		x[slideIndex-1].style.display = "block";
		setTimeout(ak_carousel, 5000); // Change image every 5 seconds
	}
	

</script>
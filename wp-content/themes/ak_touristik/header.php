<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ak_touristik
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'ak_touristik' ); ?></a>

	<header id="masthead" class="site-header">
		
		<nav id="site-navigation" class="main-navigation">
			<div class="menu-toggle ak-nav-mobile">
				<?php the_custom_logo(); ?>
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"> <span class="navbar-toggler-icon"></span></button>
			</div>
			
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
			) );
			?>
			<script>
				// Add the  logo behind the "AK Touristik" Menu Item
				// Add thee "ak-nav-logo" class to the first custom item in the menu
				document.getElementsByClassName ('menu-item-type-custom menu-item-object-custom')[0].className += ' ak-nav-logo';

				// Get the logo SRC
				<?php
				$custom_logo_id = get_theme_mod( 'custom_logo' );
				if ( $custom_logo_id ) {
					$ak_string=wp_get_attachment_image_src( $custom_logo_id, 'full', false);
				}
				?>

				// Set the logo as the background image of the first custom item in the menu
				document.getElementsByClassName ('menu-item-type-custom menu-item-object-custom')[0].style.backgroundImage = "url('<?php echo ($ak_string[0]);?>')" 
			</script>
			
		</nav><!-- #site-navigation -->
		
	</header><!-- #masthead -->

	<div id="content" class="site-content">
		
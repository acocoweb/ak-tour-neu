<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ak_touristik
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer ak-footer">
		<div class="container mt-4 mb-3">
			<div class="row footer-widgets ">
			
				<div class="col-sm-6 footer_col_1">
					<?php dynamic_sidebar( 'footer_col_1' ); ?>
				</div>
				<div class="col-sm-6 footer_col_2">
					<?php dynamic_sidebar( 'footer_col_2' ); ?>
				</div>

			</div> <!-- .footer-widgets -->
			<div class="row site-info">
				<div class="col-sm-12 footer_site_info">
					<?php dynamic_sidebar( 'footer_site_info' ); ?>
				</div>
			</div><!-- .site-info -->
		</div> <!--.container -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

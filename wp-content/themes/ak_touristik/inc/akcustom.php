<?php
/*** Custom Code for AK Touristik Theme  ***/

/** Load Label Names to Search Fields **/
function fwp_add_facet_labels() {
?>
<script>
(function($) {
    $(document).on('facetwp-loaded', function() {
        $('.facetwp-facet').each(function() {
            var $facet = $(this);
            var facet_name = $facet.attr('data-name');
            var facet_label = FWP.settings.labels[facet_name];

            if ($facet.closest('.facet-wrap').length < 1) {
                $facet.wrap('<div class="facet-wrap"></div>');
                $facet.before('<p class="ak-facet-label">' + facet_label + '</p>');
            }
        });
    });
})(jQuery);
</script>
<?php
}
add_action( 'wp_head', 'fwp_add_facet_labels', 100 );


/** FacetWP: Change date format for dates in search results ("reise-suche" Template) */

function ak_change_date_format( $value, $item ) {
    $date_mod = NULL;
    $field_name = $item['settings']['name'];
    $field_start = substr($field_name,0,6);
    
    if ( 'daten_' == $field_start && strlen($value) == 8 )
    {
        if (is_numeric($value)) {

            $date_year = substr($value,0,4);
            $date_month = substr($value,4,2);
            $date_day = substr($value,6,2);

            if ($date_year > 2000 && $date_month <=12 && $date_day <= 31) {
                $date_mod = $date_day . '.' . $date_month . '.' . $date_year;
                /* $date_mod = date_create( $value );
                $date_mod = date_format( $value, "d.m.Y" );
                */
                return $date_mod; 
            }
            else {
                return $value;
            }
        }
        else {
            return $value;
        }
    }
    else {
        return $value;
    }
}
add_filter( 'facetwp_builder_item_value', 'ak_change_date_format', 10, 2 );



/* FacetWP: Limit Post Excerpts to 280 characters for search results ("reise-suche" Template) */ 

function ak_limit_post_excerpt ( $string, $item ) {
    if ( 'post_excerpt' == $item['source'] ) {
        if (strlen($string) > 280) 
        {
            $string = wordwrap($string, 280);
            $string = substr($string, 0, strpos($string, "\n"));
            $string = $string . '...';
        }
    }

    return $string;
}
add_filter( 'facetwp_builder_item_value', 'ak_limit_post_excerpt', 10, 2 ); 


?>
<?php
/**
 * ak_touristik functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ak_touristik
 */

if ( ! function_exists( 'ak_touristik_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function ak_touristik_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ak_touristik, use a find and replace
		 * to change 'ak_touristik' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'ak_touristik', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'ak_touristik' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'ak_touristik_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		/** Add Banner Size image for single "Reise" (Trip) */
		add_theme_support( 'pop-up-banner' );
		add_image_size('banner-image', 3840, 2160, true);
	}
endif;
add_action( 'after_setup_theme', 'ak_touristik_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ak_touristik_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'ak_touristik_content_width', 640 );
}
add_action( 'after_setup_theme', 'ak_touristik_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ak_touristik_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ak_touristik' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ak_touristik' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Reisefinder Suche', 'ak_touristik' ),
		'id'            => 'reisefinder_search',
		'description'   => esc_html__( 'Reisesuche Filter Felder', 'ak_touristik' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 1', 'ak_touristik' ),
		'id'            => 'footer_col_1',
		'description'   => esc_html__( 'Widgets Footer Column 1', 'ak_touristik' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 2', 'ak_touristik' ),
		'id'            => 'footer_col_2',
		'description'   => esc_html__( 'Widgets Footer Column 2', 'ak_touristik' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Seite Info', 'ak_touristik' ),
		'id'            => 'footer_site_info',
		'description'   => esc_html__( 'Links Footer Site Info', 'ak_touristik' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
	) );
	
	register_sidebar( array(
		'name'          => esc_html__( 'Reise Seite Bottom Info', 'ak_touristik' ),
		'id'            => 'reise-seite-bottom-info',
		'description'   => esc_html__( 'Reise Seite Untere Text', 'ak_touristik' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
	) );
	
	
}
add_action( 'widgets_init', 'ak_touristik_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ak_touristik_scripts() {
	wp_enqueue_style( 'aktouristik-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
    wp_enqueue_script( 'aktouristik-bootstrap', get_template_directory_uri() . '/js/bootstrap.bundle.min.js');
    wp_enqueue_script( 'aktouristik-jq', get_template_directory_uri() . '/js/bjquery-3.3.1.min.js');

	wp_enqueue_style( 'ak_touristik-style', get_stylesheet_uri() );

	wp_enqueue_script( 'ak_touristik-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	
	wp_enqueue_script( 'ak_touristik-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'ak_touristik_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/** Include AK-Touristik Custom Functions */
require get_template_directory() . '/inc/akcustom.php';


